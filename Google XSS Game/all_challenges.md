# XSS Game
XSS Game is a series of 8 challenges sponsored by Google which aim to teach you
about a common type of web vulnerability called XSS (Cross Site Scripting). If
you have not tried these yourself I would highly recommend taking a few hours
to give them a shot at [https://www.xssgame.com/](https://www.xssgame.com/).

----------------------------------------

## XSS 1: Foogle
> This level demonstrates a common cause of XSS where user input is directly
> written to the page without proper escaping.
>
> Interact with the vulnerable application window below and find a way to make
> it execute JavaScript of your choosing. You can take actions inside the
> vulnerable window or directly edit its URL bar.

<img src="https://i.imgur.com/A1PQrAm.png" width="800" align="center" alt="Foogle Screenshot"/>

Foogle introduces us to the simplest possible XSS vulnerability. Input from the
user is directly injected into the HTML of the page without any sanitization.
When we enter a query that only contains ascii letters, everything is fine, but
what happens if we enter some valid HTML?

```html
<script>alert()</script>
```

If we view the source of the page we can see that our input is being interpreted
as if it were normal HTML. As a result, when the query loads we get an alert box
to pop up informing us that we completed the level. (Check out
`/static/js/js_frame.js` if you're curious about what happened to the original
alert box.)

<img src="https://imgur.com/awQECEr.png" width="800" align="center" alt="Foogle Source"/>

Note that on a real site and with a modern browser, this particular attack would
not work as it would be blocked by the browser's XSS Auditor. The XSS Auditor will
actually check the content of the page against the request URL and stop it from
loading if it finds any HTML script tags. The reason our payload worked for
Foogle is because the server disabled the XSS Auditor by setting the
`x-xss-protection` header to `0` (More about this here:
[https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection)).
Although the XSS Auditor sounds great, in most cases it is still trivial to
bypass by using JavaScript event handlers like `onload` to deliver the payload:

```html
<style onload="alert()"></style>
```
Notice that we are using a `style` tag instead of a `script` tag here. A subtle
difference, however, our payload will now pass the XSS Auditor.

## XSS 2: Time's out!
> Every bit of user-supplied data must be correctly escaped for the context of
> the page in which it will appear. This level shows why.
>
> Enter an input that will cause the application to execute the alert() function
> in JavaScript.

<img src="https://i.imgur.com/8XJLz61.png" width="800" align="center" alt="Time's Out Screenshot"/>

This exercise presents us with a simple application which will set a JavaScript
timer using `setTimeout()` for the number of seconds that we enter into the
input box. When the timeout happens, it simply shows us a link to go back to the
first page and set another timer. Lets take a look at the source code and analyze
where an injection vulnerability might exist.

<img src="https://i.imgur.com/llZ0JGo.png" width="800" align="center" alt="Time's Out Source"/>

We see that this time, our input is being inserted into two places. First into
the context of an inline JavaScript event handler, and also directly into
the document's HTML. That second part looks exactly like the vulnerability from
the first exercise, so let's try the same payload again.

```html
<script>alert()</script>
```

<img src="https://i.imgur.com/mys7GOp.png" width="800" align="center" alt="Time's Out Source 2"/>

Sadly for us, this no longer works because our input is being modified by some
sort of HTML entity encoder like PHP's `htmlspecialchars()` (check out the
documentation here:
[https://secure.php.net/manual/en/function.htmlspecialchars.php](https://secure.php.net/manual/en/function.htmlspecialchars.php)).
This is actually a great way to prevent most injection vulnerabilities while
also having the page behave as one might expect. In this case we see the text
`<script>alert()</script>` rendered on the page which is probably how the
developer would want our strange input to be handled anyways.

However, notice that we said this prevents *most* injection attempts. The
`htmlspecialchars()` function is great at stopping HTML injection, but it
doesn't work in all contexts. Because our input is being injected into the
context of the `onload` attribute, the encoded HTML entities will actually be
decoded again before the JavaScript is interpreted. Check out [this StackOverflow
post](https://stackoverflow.com/questions/19584189/when-used-correctly-is-htmlspecialchars-sufficient-for-protection-against-all-x#19587643)
for more examples.

This means we are able to escape the `startTimer(' /* our input */ ')` function
even though special characters get encoded as HTML entities. Let's try to supply
some input which closes the single quoted string, ends the function call to
`startTimer()` and executes an alert. We'll also add a comment at the end
to prevent the original closing quote and parenthesis from raising a syntax
error.
```
10');alert()//
```
Note that we need to URL encode the semicolon to get our browser to send the
full payload: `10')%3Balert()//`. When we submit this, we get the alert, and
inspecting the page using our browser's developer tools, we see how our
input is being interpreted, despite the HTML encoding.

<img src="https://i.imgur.com/A3hMu93.png" width="800" align="center" alt="Time's Out Dev Tools"/>

## XSS 3: Gallery
> Complex web applications often generate parts of their UI in JavaScript. Some
> common JS functions are execution sinks, which means that they will cause the
> browser to execute any scripts that appear in their input.
>
> The application on this level is using one such sink.

<img src="https://i.imgur.com/ChUgRS8.png" width="800" align="center" alt="Gallery Screenshot"/>

In this exercise our input is no longer being sent directly to the server. There
are no input boxes, and all we get to do is click the tabs to view a selection
of cute cat pictures. So where is the injection? When we click the tabs, we
notice that the url changes. It looks like the app is using the fragment
identifier (`#`) to decide which tab to load. The fragment identifier was
historically used to tell the web browser where on a page to scroll, but on the
modern web, that idea has been expanded, and JavaScript applications can use it
to denote arbitrary information about the sub resources on the page. The
important thing to remember, is that this portion of the URL is never actually
send to the server. It is purely there for the client.

Lets take a look at how this page is implementing the tab by fragment identifier
feature.

<img src="https://i.imgur.com/QwhClFK.png" width="800" align="center" alt="Gallery Source"/>

The first thing we notice is that the `name` parameter gets parsed to a number
using `parseInt()`. This is an important thing to look for when identifying
XSS Vulnerabilities, because we know that the the output of parsing functions
like this will usually have a different type, and therefore may prevent us from
injecting arbitrary characters somewhere. In this case we get lucky, because the
developer doesn't use `parseInt()` when creating the `img` tag. This means we
have our arbitrary HTML injection again (no need to worry about
`htmlspecialchars()` because this input is never sent to the server, although
the browser's URL encoding will have a similar effect).

Since our input is being injected into the context of an image tag's HTML
attributes, we can use the `onload` method from earlier to get JavaScript
execution. All we have to do is send a single quote (luckily these don't get
URL encoded) to escape the `src` attrubite and then we can add our alert.

```
#1.jpg'onload='alert()'
```

Note that we can't use spaces because they get URL encoded to `%20`, and we get
a weird image attribute `.jpg=""`. We can clean it up a little bit by adding
a `data-*` attribute to capture the junk that is added after our payload. This
isn't strictly necessary, but feels a bit cleaner.

```
#1.jpg'onload='alert()'data-null='
```

## XSS 4: Google Reader
> Cross-site scripting isn't just about correctly escaping data. Sometimes,
> attackers can do bad things even without injecting new elements into the DOM.

<img src="https://i.imgur.com/masZue5.png" width="800" align="center" alt="Google Reader Screenshot"/>

This application consists of a few pages which let us "sign up" for the
exclusive beta of Google Reader 2.0. We see an input box for our email, so we
immediately think there could be some injection there, but when we view the
source code we realize that this is not the case.

<img src="https://i.imgur.com/gZxFgyF.png" width="800" align="center" alt="Google Reader Source"/>

However, there are is an interesting query parameters we can play around with.
It looks like several of the pages use a parameter called `next` to indicate
where the link to the next page should point to. Unfortunately (and as the
description suggests), this parameter is well encoded and we are unable to
escape the `href` attribute. But how about on the confirm page?

<img src="https://i.imgur.com/GWktDFX.png" width="800" align="center" alt="Google Reader Source 2"/>

We still have the `next` query parameter, but this time the page will redirect
us to any arbitrary URL that we give it. This is a bad idea for many reasons,
for example we could send someone a link which appears to go to a trusted site,
but then redirects to our evil site. However, executing JavaScript on the user's
browser via our own domain is not quite as powerful as an XSS vulnerability
which will allow us to execute malicious JavaScript from a trusted domain. The
main difference is that on a trusted domain, JavaScript has access to the
user's cookies which can contain valuable secrets such as authentication tokens.

So how can we execute JavaScript when all we have control over is the browser
location? It feels as though all we can do is redirect the browser to different
URL's, but actually `window.location` allows us to do much more. We don't just
have to pass in a URL, we could pass in any *URI*. It's easy to forget that
URI's let us describe many different schemes, not just `http:` and `https:` (I
recommend checking out the Wikipedia article on this:
[https://en.wikipedia.org/wiki/Uniform_Resource_Identifier](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)).
It turns out that there is also a scheme for executing JavaScript
(`javascript:`). This was actually used quite often (although it is now widely
considered to be bad practice) to add dynamic actions to hyperlinks, but prevent
them from scrolling the page around.

```html
<a href="javascript:void(0)" id="login">login</a>
```

So our solution is pretty simple. Just tell the browser to execute our payload
through a `javascript:` URI.

```
/confirm?next=javascript:alert()
```

## XSS 5: Angular
> Angular is a very popular framework that has its own set of rules when it
> comes to securely developing applications. One of these is that you should be
> careful when modifying the DOM before Angular's templating system runs.
>
> This challenge demonstrates why this is important.

<img src="https://i.imgur.com/X2SiRSO.png" width="800" align="center" alt="Angular Screenshot"/>

Angular is a JavaScript framework which makes it easy to create very dynamic
web pages. The classic "Hello World" Angular example is basically what we have
in this challenge. A simple text box, which changes content somewhere on the
page as you type. If we view the source code, we see that the code is simpler
than we might expect.

<img src="https://i.imgur.com/fZAdGWd.png" width="800" align="center" alt="Angular Source"/>

That's because Angular has it's own templating system which modifies the page
after it has loaded, and continues to update the page while the user is
interacting with it. Anything enclosed in double curly braces `{{ }}` will be
evaluated by the Angular engine, and updated dynamically.

This app looks similar to the one from the first challenge, so lets try the
Angular equivalent of the payload we used there.

```
{{ alert() }}
```

Unfortunately it just prints out our query, as if it were text. When we check
the source code again and find the element where query results are displayed,
we notice that the `<p>` tag has an attribute called `ng-non-bindable`.

<img src="https://i.imgur.com/ZQq36v6.png" width="800" align="center" alt="Angular Source 2"/>

That seems suspiciously like an Angular feature designed to prevent this sort of
XSS. When we check the Angular documentation on this directive, it confirms
that this is the case:
[https://docs.angularjs.org/api/ng/directive/ngNonBindable](https://docs.angularjs.org/api/ng/directive/ngNonBindable).

However, the `query` parameter is not the only injection point. When we read the
snippet of JavaScript which set's up the Angular controller we also notice that
it seems to be supporting some UTM (Urchin Tracking Module) parameters.

<img src="https://i.imgur.com/HsjqLVJ.png" width="800" align="center" alt="Angular Source 3"/>

Urchin was the predecessor of Google Analytics and helped marketers track what
people were doing on websites. The important thing for us, is that the page is
passing these parameters (which arrive in the query string) on as POST
parameters when we hit "search" by inserting them into the `value` attribute
of correspondingly named hidden fields. This means we have complete control over
the contents of the `value` attributes for any of the existing UTM hidden
fields.

We could try to escape the quotes of the `value` attribute, but unfortunately
because the content is being set dynamically through a JavaScript variable, this
doesn't work at all. There is a way we can exploit this however, and it comes
down to the fact that Angular checks the *DOM* for template bindings, not the
HTML.

Even though the UTM values will never appear in the HTML, Angular will still
check them for template bindings. Let's try it out:

```
?utm_term={{alert()}}
```

Sure enough, the page now continually pop's alert boxes because Angular checks
for template bindings in a never ending loop.

## XSS 6: Angular 2

> A programming pattern that often leads to Angular expression injection is
> using a server side templating system to generate the HTML which Angular uses
> as its own template. This is true even if the server side template guarantees
> that there is no "ordinary" XSS in the output.

<img src="https://i.imgur.com/ArmTxbr.png" width="800" align="center" alt="Angular 2 Screenshot"/>

This challenge looks very similar to the last one, just without the UTM
parameters. When we search for something, our input is HTML encoded and put into
an `ng-non-bindable` element. We also get a few hints, because at first glance
it doesn't look like we can do much with this app. The last hint tells us that
there are multiple injection points, so let's take a close look at the source
code for any place the page could be using data from the URL.

<img src="https://i.imgur.com/1eRv1gc.png" width="800" align="center" alt="Angular 2 Source"/>

In the previous challenge, the form action was blank, meaning that the form
should submit data to the current page, however, in this challenge the form
action appears to be hard coded.
