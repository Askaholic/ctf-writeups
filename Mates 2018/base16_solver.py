import string

ALPHABET = string.ascii_uppercase[0:16]


def int_to_bin(num):
    partial = bin(num)[2:]
    return "0" * (4 - len(partial)) + partial


def bin_to_hex(binstr):
    i = 0
    resultstr = ""
    while i < len(binstr):
        resultstr += hex(int(binstr[i: i+4], 2))[2:]
        i += 4
    return resultstr


def hex_to_chrs(binstr):
    i = 0
    resultstr = ""
    while i < len(binstr):
        resultstr += chr(int(binstr[i:i+2], 16))
        i += 2
    return resultstr


def decode(cipher):
    bits = ""
    for c in cipher:
        num = ALPHABET.find(c)
        bits += int_to_bin(num)
    return bits


def xor(binstr1, binstr2):
    return ''.join([str(int(b1) ^ int(b2)) for b1, b2 in zip(binstr1, binstr2)])


def undo_mangle(binstr):
    i = 0
    resultstr = ""
    prev_chunk = None
    while i < len(binstr):
        chunk = binstr[i+8:i+16]
        if prev_chunk:
            resultstr += xor(chunk, prev_chunk)
        else:
            resultstr += chunk

        prev_chunk = chunk
        i += 16
    return resultstr


if __name__ == '__main__':
    print(ALPHABET)
    while True:
        print("Cipher: ", end='')
        cipher = input()
        res = decode(cipher)
        # print(res)
        res = undo_mangle(res)
        # print(res)
        res = bin_to_hex(res)
        # print(res)
        print(hex_to_chrs(res))

# 11000010 01100111 10100101 00000000 11000010 01100111 10100101 00000000
# 01100111 00000000 01100111 00000000

# ABCD
# 11100100 01000001 10100110 00000011 11100101 01000000 10100001 00000100
# 01000001 00000011 01000000 00000100

# ABCDE
# 11100100 01000001 10100110 00000011 11100101 01000000 10100001 00000100 11100100 01000001
