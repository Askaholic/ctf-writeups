from pwn import *
import hashpumpy
import re


if __name__ == '__main__':
    r = remote("13.251.110.215", 10001)

    r.recvuntil(":")  # "Your Wallet"
    r.recvuntil("4")
    r.recvline()
    r.send("2\n")
    r.recvuntil(":")
    r.recvuntil(":")

    r.send("5\n")
    r.recvline()
    order = r.recvline().decode().strip()
    print("got order: ", order)
    (orig_data, signature) = re.match(r"(.*)&sign=([a-f0-9]+)", order).groups()

    for i in range(8, 32):
        (new_hash, new_message) = hashpumpy.hashpump(signature, orig_data, "&price=0", i)

        r.recvuntil(":")  # "Your Wallet"
        r.recvuntil("4")
        r.recvline()
        r.send("3\n")
        r.recvuntil(":")
        r.recvuntil(":")
        r.send(new_message + b"&sign=" + new_hash.encode() + b"\n")
        print("Sent")
        r.recvline()
        resp = r.recvline().decode().strip()
        print(resp)
        if "invalid" not in resp.lower():
            r.interactive()
