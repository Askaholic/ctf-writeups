# Vittel Store [100]
>  Thank God It's Weekend! Let's go shopping!
>
> Source: https://drive.google.com/file/d/167se6uAZ48Bt5k34m37LOK1tD2wt3Tsb/view?usp=sharing
>
> nc 13.251.110.215 10001

I knew what this challenge was going to be pretty much as soon as I saw the
words "crypo" and "store" plus the low point value. There was a challenge
basically exactly like this one during RCTF called "CPU Shop" where you had to
buy CPU's instead of phones.

The challenge is that we need to purchase the flag, but we don't have enough
money. However, purchasing is done in 2 steps. First we create an order which
is cryptographically (well kindof) signed by prepending a secret and then hashing
the whole string using some sort of SHA variant.

```python
payment = 'product=%s&price=%d&timestamp=%d' % (items[n][0], items[n][1], time.time()*1000000)
sign = sha256(signkey+payment).hexdigest()
payment += '&sign=%s' % sign
print 'Your order:\n%s\n' % payment
```

Then we give this order and signature back during checkout, and the checkout
function verifies that the order was correctly signed with the secret.

```python
payment = raw_input().strip()
sp = payment.rfind('&sign=')
# ...
sign = payment[sp+6:]
payment = payment[:sp]
signchk = sha256(signkey+payment).hexdigest()
if signchk != sign:
	print 'Invalid Order!'
	return

for k,v in parse_qsl(payment):
# ... price is set here

if money < price:
	print 'Sorry, you don\'t have enough money'
	return

money -= price
print 'Your current money: $%d' % money
print 'You have bought %s' % product
if product == 'FLAG':
	print 'Good job! Here is your flag: %s' % open('flag').read().strip()
```

Because SHA is vulnerable to a length extension attack we are able to modify the
order string and create a new valid signature for it without ever needing to
know the secret.

The caveat is that we can only add more data to the end of the order string, and
this is why the challenge uses a URL format to create the order string. If we
can order the flag, append `&price=0` (you could even put a negative number here
and gain some credits) and create a valid hash for this new string, then the
URL parser will always take the last occurrence of `price` as the correct value.

But how to get a valid signature? Luckily there are tools (with python
bindings!) out there to do this for us (see
[HashPump](https://github.com/bwall/HashPump)). You don't even need to tell
HashPump which algorithm is being used, it will figure it out from the hash
length. So the challenge basically becomes writing a script to do some automatic
interaction with the server.

One other small catch is that we need to know the length of the secret, and the
server chooses a random length between 8 and 32. So instead of being able to
hardcode the secret length, we just have to brute force it, or simply connect
a bunch of times with a 1/24 chance of the server picking the same length that
we chose.

```python
import hashpumpy

for i in range(8, 32):
    (new_hash, new_message) = hashpumpy.hashpump(signature, orig_data, "&price=0", i)
```
You can check out the full script on [GitLab](https://gitlab.com/Askaholic/ctf-writeups/blob/master/Mates%202018/solve_store.py).

Because I'm on satellite internet and have about 700ms ping to anything, I was
only able to get to about 5 or 6 password lengths before the server timed out
and disconnected me, so I just had to run the script a bunch of times until I
got lucky.

```
You have bought FLAG
Good job! Here is your flag: matesctf{e4sy_3xt3nti0n_4tt4cK_x0x0}
```

As a side note, if you want to learn how length extension actually works I would
highly recommend writing your own implementation during your spare time. It's
actually pretty easy and you definitely wont feel so bad about mindlessly
throwing HashPump at a challenge like this in the future. Start by writing your
own implementation of your favorite hash algorithm (MD5 and any SHA variant are
vulnerable to length extensions) and by the time you finish this, you will
probably have a good idea of how to do the length extension. And remember that
your implementation doesn't have to be fast, it just has to be correct so python
or other scripting languages are totally ok.
